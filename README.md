<h1>Git and Robot Automation Training</h1>
<h2>Pre-work</h2>

We will have a short discussion and hands-on activity on how to use Git.
Please make sure that the following requirements are met.

<h3>Open a command-line terminal</h3>
- For Windows users, please download GitBash for Windows and install it on your laptop. This will serve as your terminal.
- For Mac users, a program named Terminal is readily available on your laptop


<h3>Git Pre-req</h3>
You need:
- to download and install git (https://git-scm.com/download/win)
- a registered gitlab account* (please check your inbox; you'll receive a separate invite email)


Once you are done installing git and have created a gitlab account, you can start by generating SSH keys.

<h3>Generate SSH keys</h3>

- Open terminal
- On the terminal window, execute the following:

> ssh-keygen -t rsa -b 4096 -C "your_email@example.com"

> [Press Enter Key]

> [Type a passphrase]

> [Type passphrase again]

> eval "$(ssh-agent -s)"

> ssh-add ~/.ssh/id_rsa


For detailed guide and explanation on how to (and why) generate SSH keys, view it here:
* https://help.github.com/articles/generating-ssh-keys/ or
* http://doc.gitlab.com/ce/ssh/README.html or
* https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2

Now, let's add the generated public key to our Gitlab accounts


On terminal, execute command:
> clip < ~/.ssh/id_rsa.pub      [this would save content of ssh file 'id_rsa.pub ' to clipboard]


On web browser, Go to gitlab.com > Profile settings > SSH keys
- Click the 'Add SSH Key' button
- Paste / Ctrl+V the copied public key to KEY field
- Type any title on the TITLE field (eg: window ssh key)
- Click the "Add Key" button


Then, let's try cloning the sample project I've created (project name: gitchoo) by doing the following:
- First, let's access the sample project on our web browser (https://gitlab.com/yohanate/gitchoo)
- Select SSH (not HTML), then copy the displayed ssh link to clipboard (the ssh link looks something like this: git@gitlab.com:sampleuser/sampleprojectname.git)


On our terminal window, let's create a folder (eg: gitlabfolder) where we can store all our gitlab projects:
> mkdir gitlabfolder

> cd gitlabfolder

Now that we're inside the 'gitlabfolder', execute command:
> git clone 'paste-the-ssh-link-here'

> [Type 'yes' then press Enter Key]

After performing the git clone, let's check if everything is working
> cd gitchoo

> sh checkifworking.sh


- If there are errors encountered, please check if previous instructions have been followed.
- Else if no errors were encountered, then we're all set.

That's all folks.

Thanks!